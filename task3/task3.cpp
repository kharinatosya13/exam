﻿#include <iostream>
#include "descending_sequence.h"

int main()
{
    std::cout << findDescendingSequence("zyxelea") << std::endl;
    std::cout << findDescendingSequence("abccdb") << std::endl;
    std::cout << findDescendingSequence("zyxwcba") << std::endl;

    std::string word;
    std::cin >> word;
    while (word != "exit")
    {
        std::cout << findDescendingSequence(word) << std::endl;
        std::cin >> word;
    }
}
