#include "pch.h"
#include "../task3/descending_sequence.h"

descendingSequence result;

TEST(descendingSequenceTests, zyxelea) {

	result.size = 4;
	result.first_elem = 'z';
	result.second_elem = 'e';

	EXPECT_EQ(findDescendingSequence("zyxelea"), result);
}

TEST(descendingSequenceTests, abccdb) {

	result.size = 2;
	result.first_elem = 'd';
	result.second_elem = 'b';

	EXPECT_EQ(findDescendingSequence("abccdb"), result);
}

TEST(descendingSequenceTests, zyxwcba) {

	result.size = 7;
	result.first_elem = 'z';
	result.second_elem = 'a';

	EXPECT_EQ(findDescendingSequence("zyxwcba"), result);
}

TEST(descendingSequenceTests, a) {

	result.size = 1;
	result.first_elem = 'a';
	result.second_elem = 'a';

	EXPECT_EQ(findDescendingSequence("a"), result);
}

TEST(descendingSequenceTests, aaaaaaa) {

	result.size = 1;
	result.first_elem = 'a';
	result.second_elem = 'a';

	EXPECT_EQ(findDescendingSequence("aaaaaaa"), result);
}

TEST(descendingSequenceTests, azazazaza) {

	result.size = 2;
	result.first_elem = 'z';
	result.second_elem = 'a';

	EXPECT_EQ(findDescendingSequence("azazazaza"), result);
}