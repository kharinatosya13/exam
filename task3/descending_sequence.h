#pragma once
#include <iostream>

struct descendingSequence {
	int size = 0;
	char first_elem = ' ';
	char second_elem = ' ';
};

descendingSequence findDescendingSequence(const std::string& word) {
	std::string sequence_temp = "";
	std::string sequence_res = "";
	sequence_res += word[0];
	sequence_temp = sequence_res;
	
	for (int i = 1; i < word.size(); i++)
	{
		if (word[i-1] > word[i]) {
			sequence_temp += word[i];
		}
		else {
			i -= sequence_temp.size() - 1;
			if (sequence_temp.size() > sequence_res.size())
			{
				sequence_res = sequence_temp;
			}
			sequence_temp.clear();
			sequence_temp += word[i];
		}
	}

	if (sequence_temp.size() > sequence_res.size())
	{
		sequence_res = sequence_temp;
	}

	descendingSequence result;
	result.size = sequence_res.size();
	result.first_elem = *(sequence_res.begin());
	result.second_elem = *(--sequence_res.end());

	return result;
}

std::ostream& operator<<(std::ostream& os, const descendingSequence& s)
{
	os << "size: " << s.size << "\tfirst elem: " << s.first_elem << "\tsecond elem: " << s.second_elem;
	return os;
}

bool operator==(const descendingSequence& s_1, const descendingSequence& s_2) {
	return s_1.size == s_2.size && s_1.first_elem == s_2.first_elem && s_1.second_elem == s_2.second_elem;
}